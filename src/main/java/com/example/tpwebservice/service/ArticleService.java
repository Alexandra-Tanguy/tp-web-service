package com.example.tpwebservice.service;

import com.example.tpwebservice.models.Article;

import java.util.List;

public interface ArticleService {

    List<Article> getAll();

    Article getById(Integer id);

    Article createArticle(Article article);

    void deleteArticle(Integer id);

    Article update(Article article);
}

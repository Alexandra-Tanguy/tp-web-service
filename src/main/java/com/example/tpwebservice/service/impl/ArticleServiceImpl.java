package com.example.tpwebservice.service.impl;

import com.example.tpwebservice.exception.UnknowResourceException;
import com.example.tpwebservice.models.Article;
import com.example.tpwebservice.repository.ArticleRepository;
import com.example.tpwebservice.service.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    Logger log = LoggerFactory.getLogger(ArticleServiceImpl.class);

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public List<Article> getAll() {
        return this.articleRepository.findAll(Sort.by("designation").ascending());
    }

    @Override
    public Article getById(Integer id) {
        return this.articleRepository.findById(id)
                .orElseThrow(() -> new UnknowResourceException("article inconnu."));
    }

    @Override
    public Article createArticle(Article article) {
       log.debug("tentative de creation d'article");
        return this.articleRepository.save(article);
    }

    @Override
    public void deleteArticle(Integer id) {
        Article articleToDelete = this.getById(id);
        this.articleRepository.delete(articleToDelete);
    }

    @Override
    public Article update(Article article) {
        log.debug("tentative de modificaion de l'article", article.getId());
        Article modifArticle = this.getById(article.getId());
        modifArticle.setDesignation(article.getDesignation());
        modifArticle.setPrix(article.getPrix());
        modifArticle.setQuantite(article.getQuantite());
        return this.articleRepository.save(modifArticle);
    }
}

package com.example.tpwebservice.service.impl;

import com.example.tpwebservice.exception.UnknowResourceException;
import com.example.tpwebservice.models.Article;
import com.example.tpwebservice.models.Commande;
import com.example.tpwebservice.repository.CommandeRepository;
import com.example.tpwebservice.service.ArticleService;
import com.example.tpwebservice.service.CommandeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandeServiceImpl implements CommandeService {

    Logger log = LoggerFactory.getLogger(CommandeServiceImpl.class);


    @Autowired
    private ArticleService articleService;
    @Autowired
    private CommandeRepository commandeRepository;

    @Override
    public List<Commande> getAll() {
        return this.commandeRepository.findAll();
    }

    @Override
    public Commande getById(Integer id) {
        return this.commandeRepository.findById(id)
                .orElseThrow(() -> new UnknowResourceException("commande inconnue."));
    }

    @Override
//    methode pour diminuer la quantité article en fonction de la quantité commandée lors de la creation commande
    public Commande createCommande(Commande commande) {
        log.debug("tentative de creation de commande");
        List<Article> articles = commande.getArticles();
        for (Article article : articles) {
            int quantiteCommandee = commande.getQuantite();
            if (article.getQuantite() >= quantiteCommandee) {
                article.setQuantite(article.getQuantite() - quantiteCommandee);
                articleService.update(article);
            } else {
                throw new IllegalArgumentException("Quantité insuffisante pour le produit" + article.getDesignation());
            }
        }
        return commandeRepository.save(commande);
    }

    public Commande addArticle(Commande commande, Article article){
        //TODO Implémenter cette méthode voir interface CommandeService
        // Elle serait ensuite implémenter dans la méthode createCommande plus haut
        commande.getArticles().add(article);
        return null;
    }
}


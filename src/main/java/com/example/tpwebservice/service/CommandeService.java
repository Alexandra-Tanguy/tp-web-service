package com.example.tpwebservice.service;

import com.example.tpwebservice.models.Article;
import com.example.tpwebservice.models.Commande;

import java.util.List;

public interface CommandeService {

    List<Commande> getAll();

    Commande getById(Integer id);

    Commande createCommande(Commande commande);

     Commande addArticle(Commande commande, Article article);
        //TODO Implémenter une méthode qui ajoute un article à une commande qui serait appeller dans l'api de création d'une commande
        // Cela induirait qu'une commande ne peux pas exister sans un article tbh

}

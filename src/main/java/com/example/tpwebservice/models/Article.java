package com.example.tpwebservice.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 200)
    private String designation;

    @Column
    private Integer quantite;

    @Column(columnDefinition = "NUMERIC")
    private Double prix;

    @ManyToMany
    @JoinTable(
            name = "article_commande",
            joinColumns = @JoinColumn(name = "article_id"),
            inverseJoinColumns = @JoinColumn(name = "commande_id"))
    private Set<Commande> commande;



}

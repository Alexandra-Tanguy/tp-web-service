package com.example.tpwebservice.exception;

public class UnknowResourceException extends RuntimeException{

    public UnknowResourceException() {
        super("Ressource inconnue.");
    }


    public UnknowResourceException(String message) {
        super(message);
    }
}

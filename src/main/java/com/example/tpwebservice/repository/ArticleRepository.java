package com.example.tpwebservice.repository;

import com.example.tpwebservice.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
}

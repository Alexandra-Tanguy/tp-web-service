package com.example.tpwebservice.repository;

import com.example.tpwebservice.models.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandeRepository extends JpaRepository<Commande, Integer> {
}

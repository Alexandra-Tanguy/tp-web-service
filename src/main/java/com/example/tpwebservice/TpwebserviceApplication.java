package com.example.tpwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpwebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpwebserviceApplication.class, args);
	}

}

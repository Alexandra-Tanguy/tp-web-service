package com.example.tpwebservice.api.dto;

import com.example.tpwebservice.models.Article;
import lombok.*;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommandeDto {
    private Integer id;
    private Integer quantite;
    List<ArticleDto> articles;

}

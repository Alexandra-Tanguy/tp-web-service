package com.example.tpwebservice.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ArticleDto {

    private Integer id;
    private String designation;
    private Integer quantite;
    private Double prix;
    private List<CommandeDto> commande;


}

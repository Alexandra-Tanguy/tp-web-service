package com.example.tpwebservice.api.v1;


import com.example.tpwebservice.api.dto.ArticleDto;
import com.example.tpwebservice.exception.UnknowResourceException;
import com.example.tpwebservice.mapper.ArticleMapper;
import com.example.tpwebservice.service.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/article")
public class ArticleApi {

    Logger log = LoggerFactory.getLogger(ArticleApi.class);

    private final ArticleService articleService;
    private final ArticleMapper articleMapper;

    public ArticleApi(ArticleService articleService, ArticleMapper articleMapper) {
        this.articleService = articleService;
        this.articleMapper = articleMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<ArticleDto>> getAll() {
        return ResponseEntity.ok(
                articleService.getAll().stream()
                        .map(articleMapper::mapArticleToArticleDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ArticleDto> getArticleById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(articleMapper.mapArticleToArticleDto(articleService.getById(id)));
        } catch (UnknowResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ArticleDto> createArticle(@RequestBody ArticleDto articleDto) {
        log.debug("tentative de creation d'article");
        ArticleDto articleCreated = articleMapper.mapArticleToArticleDto(articleService.createArticle(articleMapper.mapArticleDtoToArticle(articleDto)));
        return ResponseEntity.created(URI.create("/v1/article/" + articleCreated.getId()))
                .body(articleCreated);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable final Integer id) {
        try {
            log.debug("suppression de l'article", id);
            articleService.deleteArticle(id);
            return ResponseEntity.noContent().build();
        } catch (UnknowResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ArticleDto> updateArticle(@RequestBody ArticleDto articleDto, @PathVariable final Integer id) {
        try {
            articleDto.setId(id);
            ArticleDto updatedArticle = articleMapper.mapArticleToArticleDto(articleService.update(articleMapper.mapArticleDtoToArticle(articleDto)));
            return ResponseEntity.ok(updatedArticle);
        } catch (UnknowResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


}


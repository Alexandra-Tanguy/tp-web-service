package com.example.tpwebservice.api.v1;

import com.example.tpwebservice.api.dto.CommandeDto;
import com.example.tpwebservice.exception.UnknowResourceException;
import com.example.tpwebservice.mapper.CommandeMapper;
import com.example.tpwebservice.service.CommandeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/commande")
public class CommandeApi {
    Logger log = LoggerFactory.getLogger(CommandeApi.class);

    private final CommandeService commandeService;
    private final CommandeMapper commandeMapper;

    public CommandeApi(CommandeService commandeService, CommandeMapper commandeMapper) {
        this.commandeService = commandeService;
        this.commandeMapper = commandeMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<CommandeDto>> getAll() {
        return ResponseEntity.ok(
                commandeService.getAll().stream()
                        .map(commandeMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/id", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CommandeDto> getCommandeById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(commandeMapper.mapToDto(commandeService.getById(id)));
        } catch (UnknowResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CommandeDto> createCommande(@RequestBody CommandeDto commandeDto) {
        log.debug("tentative de creation d'une commande");
        CommandeDto commandeCreated = commandeMapper.mapToDto(commandeService.createCommande(commandeMapper.mapToModel(commandeDto)));
        return ResponseEntity.created(URI.create("/v1/commande/" + commandeCreated.getId()))
                .body(commandeCreated);
    }
}
package com.example.tpwebservice.mapper;

import com.example.tpwebservice.api.dto.CommandeDto;
import com.example.tpwebservice.models.Commande;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CommandeMapper {
    CommandeDto mapToDto(Commande commande);

    Commande mapToModel(CommandeDto commandDto);
}




package com.example.tpwebservice.mapper;

import com.example.tpwebservice.api.dto.ArticleDto;
import com.example.tpwebservice.models.Article;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;


@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ArticleMapper {

    ArticleDto mapArticleToArticleDto(Article article);

    Article mapArticleDtoToArticle(ArticleDto articleDto);
}

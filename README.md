# Installation du projet et utilisation de la commande postman

1. Cloner le projet
2. Lancer docker
3. Run le docker-compose pour créer la db
4. Lancer l'application

## Requête postman
1. La collection est disponible sous la format du fichier tp_gestion_article.postman_collection à la racine du projet
2. L'importer dans postman via l'option en haut à gauche


## Evolution future
* Finaliser l'implémentation des commandes  méthode d'ajout des articles à une commande manquante atm
* Compléter la collection postman pour ces dernières


______________________________________________

### Petit cadeau pour la doc angular offerte !
>Stay Hydrated

>Always search for [BANGERS](https://www.youtube.com/watch?v=YWbvEOWiR_M)

>Don't let your meme be dreams